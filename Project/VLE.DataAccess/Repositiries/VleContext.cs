﻿using System.Data.Entity;
using VLE.Models;

namespace VLE.DataAccess.Repositories
{
    public class VleContext : DbContext
    {
        public VleContext() : base("VleDb")
        {
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<VleContext>());
        }

        public DbSet<Client> Clients { get; set; }
        public DbSet<Course> Courses { get; set; }

    }
}
